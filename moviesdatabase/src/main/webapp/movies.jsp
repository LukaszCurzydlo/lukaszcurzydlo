<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<c:import url="header.jsp"/>

<table width="100%" border=1>
<thead>
<th>
    ID
</th>
<th>
    Nazwe
</th>
<th>
    akcje
</th>

</thead>
<tbody>

    <c:forEach items ="${movies}" var ="movie">
        <tr>
        <td><c:out value ="${movie.id}"/> </td>
        <td><c:out value ="${movie.name}"/> </td>

        <td> <a href="MovieServlet?action=edit&id=<c:out value="${movie.id}"/>">Edytuj</a>
             <a href="MovieServlet?action=delete&id=<c:out value="${movie.id}"/>">Usun</a> </td>

        </tr>
    </c:forEach>


</tbody>
</table>
<c:import url="footer.jsp"/>


