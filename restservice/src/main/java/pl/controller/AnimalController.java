package pl.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.model.Animal;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


@CrossOrigin
@RestController
public class AnimalController {
private final AtomicLong counter = new AtomicLong();

@RequestMapping({"/","animal"})
public String animal() {
    return counter.incrementAndGet() + ". Witaj,jezu";
}

@RequestMapping("/elephant")
public Animal showAnimal(){
     Animal elephant=new Animal(0,"Elephant0");
     return elephant;
}

    @RequestMapping("/crocodile")
    public Animal showAnimal1(){
        Animal crocodile=new Animal(0,"Elephant1");
        return crocodile;
    }

    @RequestMapping("animals")
    public List<Animal> animals (){
        List<Animal> animals = new LinkedList<>();
        animals.add(new Animal (1,"jezyk"));
        animals.add(new Animal (2,"slonik"));
        animals.add(new Animal (3,"krokodylek"));
        return animals;

    }

    @RequestMapping("/mammals")
    public Set<Animal> mammals(){
        Set<Animal> animals = new HashSet<>();
        animals.add(new Animal (4,"hass"));
        animals.add(new Animal (5,"trrr"));
        return animals;

    }
@RequestMapping("/reptiles")
    public Map<String,Animal> reptiles(){
        Map<String,Animal> animals = new HashMap<>();
    animals.put( "zaba",new Animal(6,"jezyk"));
    animals.put( "zaba5",new Animal(7,"jeeeezyk"));
        return animals;
}


}
