package pl.model;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class Animal {

    @JsonIgnore
    private int id;

    private String name;

    public Animal(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public Animal() {
    }

    public int getId() {
        return id;
    }

    public Animal setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }
}
