package Resolver;

import Cities.connector.DatabaseConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Delete {

    public static boolean Insert(Map<String, String> props) {

        boolean b = false;
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        String q = "INSERT INTO " + "Student " + "( ";
        String query = "";
        String query2 = "";


        for (Map.Entry<String, String> e : props.entrySet()) {

            list2.add(e.getKey());
            list1.add(e.getValue());

        }
        for (String a : list1) {
            query +=   a +   ",";

        }


        query = query.substring(0, query.length() - 1);

        query += " ) `VALUES` ( ";

        for (String c : list2) {
            query2 += "\"" + c + "\"" + ",";

        }

        query += query2.substring(0, query2.length() - 1);
        query += " );";


        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String queryX = q + query;
            PreparedStatement ps = c.prepareStatement(queryX);

            int countRows = ps.executeUpdate();

            ps.close();
            if (countRows > 0) {
                b = true;
            } else {
                b = false;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return b;
    }


    public static void main(String[] args) {

        Map<String, String> map = new HashMap<>();
        map.put("name", "lukasz");
        map.put("surname", "curzydlo");

        System.out.println(Delete.Insert(map));
    }

}


