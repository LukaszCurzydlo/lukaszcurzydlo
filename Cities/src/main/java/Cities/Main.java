package Cities;

import Cities.connector.DatabaseConnector;
import Cities.model.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {



        List<City> listofCities = new ArrayList<>();


        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("INSERT INTO `city` VALUES (NULL, 'Wronki')");
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("UPDATE `city` SET `city`= 'Warszawa' WHERE `city` LIKE 'Wawa'");
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM `city` WHERE `city` LIKE 'Wronki'");
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM `city`";
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                System.out.println(rs.getInt("id") + "." + rs.getString("city"));
                listofCities.add(new City(rs.getInt("id"),rs.getString("city")));
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for (City c : listofCities){
            System.out.println("> " + c.getCity());
        }
    }
}