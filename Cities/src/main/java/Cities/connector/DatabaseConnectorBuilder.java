package Cities.connector;


public class DatabaseConnectorBuilder {
    private String dbhost = "localhost";
    private String dbname;
    private String dbuser = "root";
    private String dbpass = "";
    private int dbport = 3306;
    public DatabaseConnectorBuilder(){}

    public String hostname(){
        return dbhost;
    }
    public DatabaseConnectorBuilder hostname(String dbhost) {
        this.dbhost = dbhost;
        return this;
    }

    public String database(){
        return dbname;
    }
    public DatabaseConnectorBuilder database(String dbname) {
        this.dbname = dbname;
        return this;
    }
public String username(){
        return dbuser;
}
    public DatabaseConnectorBuilder username(String dbuser) {
        this.dbuser = dbuser;
        return this;
    }
public String password(){
    return dbpass;
}
    public DatabaseConnectorBuilder password(String dbpass) {
        this.dbpass = dbpass;
        return this;
    }

    public int port (){
    return dbport;
    }
    public DatabaseConnectorBuilder port(int dbport) {
        this.dbport = dbport;
        return this;
    }
}
