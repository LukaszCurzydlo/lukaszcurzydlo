package User.User;

public enum UserSex {

	SEX_MALE("mezczyzna"),
	SEX_FEMALE("dziewczyna"),
	SEX_UNDEFINED("nieokreslono");
	private String name;
	
	
	UserSex(String name){
		this.name= name;
	}
	
	
	public String getName(){
		return name;
	}
	
}
