package User.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;


public class UserGenerator {

	
	
	private final String path = "Resources/";
	private User currentUser;
	
	
	public User getRandomUser(){
		
		UserSex us = UserSex.SEX_MALE;
		if(new Random().nextInt(2)==0){
			us = UserSex.SEX_FEMALE;
		}
		return getRandomUser(us);
	
	}
	
	
	public User getRandomUser(UserSex sex){
		currentUser  = new User();
		currentUser.setSex(sex);
		String name = "";
		if(sex.equals(UserSex.SEX_MALE)){
			name = getRandomLineFromFile("name_m.txt");
		}else{
			name = getRandomLineFromFile("name_f.txt");
			
		}
		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));
		currentUser.setAddress(getRandomAddress());
		getRandomAddress(currentUser);
		
		
		return currentUser;
		
		
		
	}
	private String getRandomBirthDate(){
		int[] daysInMonths={31,28,31,30,31,30,31,31,30,31,30,31};
		int year = 1890+ new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		int day =0;
		if(year%4 == 0){
			daysInMonths[1]++;
		}
				new Random().nextInt(daysInMonths[month -1]);
		 return leadZero(day) + "." + leadZero(month) + "." + year;
		
	}
	
	private String leadZero(int arg){
		if(arg <10) return "0" + arg;
		else return "" + arg;
	}
	public void getRandomAddress(){
		
		Address adr = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split("\t");
		adr.setCountry("Poland");
		adr.setCity(cityData[0]);
		adr.setZipcode(cityData[1]);
		adr.setStreet("ul. " + getRandomLineFromFile("street.txt"));
		adr.setNumber("" + (new Random().nextInt(100)+1));
		currentUser.setAddress(adr);
	}
	
	
	
	private int  countLines(String filename){
		int lines = 0;
	try(Scanner sc = new Scanner(new File(path+filename))){
		while(sc.hasNextLine()){
			lines++;
			sc.nextLine();
			
		}
		
	}
	catch(FileNotFoundException e){
		System.out.println(e.getMessage());
		
		
	}
	return lines;
	}
	
	
	public String randomNumber(int numbersElements) {
        int phoneline = 0;
        String phoneNumber = "";
        Random rand = new Random();
        for (int i = 0; i < numbersElements; i++) {
            phoneline = rand.nextInt(10);
            phoneNumber += phoneline;
        }

        return phoneNumber;
    }

 
public void randomCCN(User currentUser) {
        String[] CCN = randomNumber(16).split("");
        String[] CCNFinal = new String[] { "", "", "", "" };
        int o = 0;
        for (int i = 0; i < 4; i++) {
            CCNFinal[i] += CCN[o++];
            CCNFinal[i] += CCN[o++];
            CCNFinal[i] += CCN[o++];
            CCNFinal[i] += CCN[o++] + " ";
        }

        currentUser.setCCN((" " + CCNFinal[0] + CCNFinal[1] + CCNFinal[2] + CCNFinal[3]));
    }
	
	
	
	public String getRandomLineFromFile(String filename){
	Random ran = new Random();
	String[] tab = new String[countLines(filename)];
	
		try(Scanner sc = new Scanner(new File(path+filename))){
			for(int i=0;i<countLines(filename);i++){
				tab [i] = sc.nextLine();
			}
			}
		
		catch(FileNotFoundException e){
			System.out.println(e.getMessage());
		}
		 String S =  tab[ran.nextInt(countLines(filename))];
		 return S;
	

	}
	
	
	public String randomNumber(int numbersElements) {
        int phoneline = 0;
        String phoneNumber = "";
        Random rand = new Random();
        for (int i = 0; i < numbersElements; i++) {
            phoneline = rand.nextInt(10);
            phoneNumber += phoneline;
        }

        return phoneNumber;
    }
	
	}

