package Student;



import java.util.HashMap;
import java.util.Map;

public class University {
	private Map<Integer, Student> students = new HashMap<>();
	
	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNumber(), student);
	}
	
	public boolean studentExists(int indexNumber) {
		// Delegacja
		return students.containsKey(indexNumber);
	}
	
	public Student getStudent(int indexNumber) {
		// Delegacja
		return students.get(indexNumber);
	}
	
	public int studentsNumber() {
		return students.size();
	}
	
	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}
}
