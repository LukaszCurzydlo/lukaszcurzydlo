package Student;



public class Student {
	private final int indexNumber;
	private final String name;
	private final String surname;
	
	public Student(int indexNumber, String name, String surname) {
		this.indexNumber = indexNumber;
		this.name = name;
		this.surname = surname;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public char[] split(String string) {
	
		return null;
	}
	
	
	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + " " + s.getSurname());
		}
	
}