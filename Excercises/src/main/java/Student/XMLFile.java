package Student;

import static xml.ParseXML.parseFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xml.ParseStudentsXML;
import xml.ParseXML;

public class XMLFile implements IFile {

	@Override
	public void save(List<Student> studentsList) {
		try {
			Document doc = ParseXML.newDocument();
			createContent(doc, studentsList);
			ParseXML.saveFile(doc, "students.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createContent(Document doc, List<Student> studentsList) {
		Element students = doc.createElement("students");
		doc.appendChild(students);
		
		for(Student student : studentsList) {
			addStudent(doc, students, student);
		}
	}

	public void addStudent(Document doc, Element students, Student student) {
		Element studentElement = doc.createElement("student");
		studentElement.setAttribute("index", String.valueOf(student.getIndexNumber()));
		
		Element name = doc.createElement("name");
		name.appendChild(doc.createTextNode(student.getName()));
		studentElement.appendChild(name);
		

		Element surname = doc.createElement("surname");
		surname.appendChild(doc.createTextNode(student.getSurname()));
		studentElement.appendChild(surname);
		
		students.appendChild(studentElement);
	}

	@Override
	public List<Student> load() {
		List<Student> studentsList = new ArrayList<>();
		try {
			Document doc = ParseXML.parseFile("students.xml");
			NodeList students = doc.getElementsByTagName("student");
			for (int i = 0; i < students.getLength(); i++) {
				Node studentNode = students.item(i);
				Student s = parse(studentNode);
				studentsList.add(s);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		
		return studentsList;
	}

	private Student parse(Node studentNode) {
		Element student = (Element) studentNode;
		int index = Integer.parseInt(student.getAttribute("index"));
		String name = ParseXML.getText(student, "name");
		String surname = ParseXML.getText(student, "surname");
		return new Student(index, name, surname);
	}

}