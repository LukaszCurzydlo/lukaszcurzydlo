package Zadaniasrednia;

import java.util.ArrayList;
import java.util.List;

public class Z6 {

	public static void main(String[] args) {
		 List<Integer> list = toList(1, 2, 3, 5);
		 
		 Integer[] array = new Integer[list.size()];
	        fromListToArray(list, array);
	        for (Integer number : array) {
	            System.out.println(number);
	        }
	}
	
	public static <T> List<T> toList(T... numbers) {
        List<T> list = new ArrayList<T>();

        for (T number : numbers) {
            list.add(number);
        }
        return list;
    }
	
	public static <T> void fromListToArray(List<T> list, T[] array) {
        if (list.size() != array.length) {
            throw new IllegalArgumentException("Rozmiany s� r�ne");
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }
    }
	
	
	
}
