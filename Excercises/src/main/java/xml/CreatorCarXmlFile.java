package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreatorCarXmlFile {
	
	private Document doc;
	
	public static void main(String argv[]) {
		new CreatorCarXmlFile().create();
	}
	
	public void create() {
		try {
			doc = ParseXML.newDocument();
			createContent();
			ParseXML.saveFile(doc, "cars.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createContent() {
		Element rootElement = createCars();
		Element supercars = addSupercars(rootElement);
		ParseXML.setAttribute(doc, supercars, "company", "Ferrari");
		addCar(supercars, "formula one", "Ferrari 101");
		addCar(supercars, "sports", "Ferrari 202");
	}

	private void addCar(Element supercars, String type, String name) {
		Element carname = doc.createElement("carname");
		ParseXML.setAttribute(doc, carname, "type", type);
		carname.appendChild(doc.createTextNode(name));
		supercars.appendChild(carname);
	}

	private Element addSupercars(Element rootElement) {
		Element supercar = doc.createElement("supercars");
		rootElement.appendChild(supercar);
		return supercar;
	}

	private Element createCars() {
		Element rootElement = doc.createElement("cars");
		doc.appendChild(rootElement);
		return rootElement;
	}
}