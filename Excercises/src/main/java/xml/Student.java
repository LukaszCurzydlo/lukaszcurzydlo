package xml;





public class Student  {
	private int indexNumber;
	private String name;
	private String surname;
	
	public Student() {}
	
	public Student(int indexNumber, String name, String surname) {
		this.indexNumber = indexNumber;
		this.name = name;
		this.surname = surname;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public void setIndexNumber(int indexNumber) {
		this.indexNumber = indexNumber;
	}

	@Override
	public String toString() {
		return "Student [indexNumber=" + indexNumber + ", name=" + name + ", surname=" + surname + "]";
	}
	
	
	
	
}
