package xml;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLFile implements Ifile {

	@Override
	public void save(List<Student> studentsList) {
		 try {
	          Document doc = ParseXml.newDocument();
	         createContent(doc,studentsList);
	         ParseXml.saveFile(doc, "studentsList.xml");
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	}
	
	private void createContent(Document doc,List<Student> studentsList){
		Element students = doc.createElement("students");
		doc.appendChild(students);
		for(Student student : studentsList){
			Element studentElement = doc.createElement("student");
			ParseXml.setAttribute(doc, studentElement, "index", String.valueOf(student.getIndexNumber()));
			 studentElement.appendChild(doc.createTextNode(student.getName()));
			 students.appendChild(studentElement);
			 
			 
			
		}
		
	}
	
	public static void main(String[] args) {
		Student student = new Student(100,"lukasz","curzydlo");
		 List<Student> studentsList = new ArrayList<>();
		 for (Student student : studentsList) {
				students.put(student.getIndexNumber(), student);
			}
		 
		
	}
	
	
	@Override
	public List<Student> load() {
		Document doc = ParsXML.parseFile("")
		return null;
	}

}
