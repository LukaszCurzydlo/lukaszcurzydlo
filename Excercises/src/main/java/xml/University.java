package xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class University {
	private Map<Integer, Student> students = new HashMap<>();

	public University() {
	}

	public University(List<Student> studentsList) {

		for (Student student : studentsList) {
			students.put(student.getIndexNumber(), student);
		}
	}

	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNumber(), student);
	}

	public boolean studentExists(int indexNumber) {
	
		return students.containsKey(indexNumber);
	}

	public Student getStudent(int indexNumber) {
		
		return students.get(indexNumber);
	}

	public int studentsNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}

	public List<Student> getStudentsList() {

		List<Student> sList = new LinkedList<>();

		for (Student s : students.values()) {

			sList.add(s);

		}

		return sList;
		
	}

	public static void main(String[] args) {

		List<Student> students = new ArrayList<>();
		students.add(new Student(100, "Jan", "Kowalski"));
		students.add(new Student(101, "Jan1", "Kowalski1"));
		University u = new University(students);

		u.showAll();

		for (Student student : u.getStudentsList()) {
			System.out.println(student);
		}
	}
}