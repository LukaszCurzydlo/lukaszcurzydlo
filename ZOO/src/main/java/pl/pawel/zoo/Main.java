package pl.pawel.zoo;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;
import pl.pawel.zoo.dao.AnimalDAO;
import pl.pawel.zoo.Entity.Animal;
import pl.pawel.zoo.util.HibernateUtil;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;
import pl.pawel.zoo.Entity.Animal;
import pl.pawel.zoo.dao.AnimalDAO;
import pl.pawel.zoo.util.HibernateUtil;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;


public class Main {
    public static void main(String[] args) {
        AnimalDAO animalDAO = new AnimalDAO();

        Animal a = new Animal();
        a.setName("Słoń");
        animalDAO.insert(a);
        animalDAO.insert(new Animal("Orzeł"));
        animalDAO.insert(new Animal("Gołąb"));

        Animal slon = new Animal();
        slon = animalDAO.get(1);

        System.out.println("Pobrales id = " + slon.getId() + " " + slon.getName());

        slon.setName("PrzerobionySlon");
        animalDAO.update(slon);

        animalDAO.delete(3);

        Animal secondAnimal = new Animal();
        secondAnimal.setId(2);
        animalDAO.delete(secondAnimal);


        for (Animal anim : animalDAO.get()) {
            System.out.println(anim.getId() + ". " + anim.getName());
        }
    }
}
