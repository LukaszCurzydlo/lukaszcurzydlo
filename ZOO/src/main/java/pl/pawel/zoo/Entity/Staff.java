package pl.pawel.zoo.Entity;

import javax.persistence.*;


@Entity
@Table(name="animal1")

public class Staff {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)

        @Column (name = "id")
        private int id;

        @Column(name="name")

    private String name;

    @Column(name="lastName")
    private String lastName;

    @Column(name="age")
    private  int age;

    public Staff(int id, String name, String lastName, int age) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public Staff() {
    }

    public int getId() {
        return id;
    }

    public Staff setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Staff setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Staff setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Staff setAge(int age) {
        this.age = age;
        return this;
    }
}
