package pl.pawel.zoo.Entity;

import javax.persistence.*;


@Entity
@Table(name="animal")
public class Animal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name = "id")
    private int id;
    @Column(name="name")
    private String name;



    public Animal(String name) {
        this.name = name;

    }

    public Animal() {
    }

    public Animal(int id,String name) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Animal setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }
}
