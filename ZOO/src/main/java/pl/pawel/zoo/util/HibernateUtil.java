package pl.pawel.zoo.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * Created by RENT on 2017-07-12.
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static  SessionFactory buildSessionFactory(){
        return new Configuration().configure().buildSessionFactory();

    }



    public static SessionFactory getSessionFactory(){
        return sessionFactory;

    }

    public static Session openSession (){
        return getSessionFactory().openSession();
    }
}
