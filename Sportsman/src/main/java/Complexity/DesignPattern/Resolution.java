package Complexity.DesignPattern;

public enum Resolution {
    R1920x1080,
    R19x1080,
    R192x1080;
}
