package Complexity.DesignPattern;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {

        ScreenRecorder screen = new ScreenRecorder();

        System.out.println("Menu: options: add, list, start(record), stop(record), setProfile, exit");
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] splits = line.split(" ");
            if(splits[0].equals("add")) {
                screen.addProfile(createProfile());
                screen.listProfiles();
            } else if(splits[0].equals("start")) {
                screen.startRecording();

            } else if(splits[0].equals("stop")) {
                screen.stopRecording();

            } else if(splits[0].equals("setProfile")) {
                System.out.println("Give profile number:");
                int profile = sc.nextInt();
                screen.setProfile(profile);
            }
            else if(splits[0].equals("exit")) {
                return;
            }
        }

    }




    public static Profile createProfile() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Set codec: ");
        System.out.println(Codec.mp4 + " Or " + Codec.mp3 + " Or " + Codec.mkv + " Or "+ Codec.avi);
        String codecType = sc.nextLine();

        System.out.println("Set resoluton: ");
        System.out.println(Resolution.R1920x1080 + " Or " + Resolution.R19x1080 + " Or " + Resolution.R1920x1080 + " Or "+ Resolution.R192x1080);
        String resolutionType = sc.nextLine();

        System.out.println("Set extension: ");
        System.out.println(Extension.h264 + " Or " + Extension.h263 + " Or " + Extension.theora + " Or "+ Extension.vr8);
        String extensionType = sc.nextLine();

        Codec codec = Codec.valueOf(codecType);
        Extension extension = Extension.valueOf(extensionType);
        Resolution resolution = Resolution.valueOf(resolutionType);

        return new Profile(resolution,codec, extension);

    }
}
