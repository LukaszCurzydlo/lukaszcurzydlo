package Complexity.DesignPattern;


public class Profile {

    private Resolution resolution;
    private Codec codec;
    private Extension extension;




    public Profile(Resolution resolution, Codec codec, Extension extension) {
        this.resolution = resolution;
        this.codec = codec;
        this.extension = extension;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }

    public Codec getCodec() {
        return codec;
    }

    public void setCodec(Codec codec) {
        this.codec = codec;
    }

    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "resolution=" + resolution +
                ", codec=" + codec +
                ", extension=" + extension +
                '}';
    }
}
