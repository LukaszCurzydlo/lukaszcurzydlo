package Complexity.Sort;

import java.util.Arrays;

public class Sort {


    public static void main(String[] args) {

        int[]left ={1,3,5,};
        int[] right = {2,4,6};
        int[] data = new int[right.length*2];
        System.out.println(Arrays.toString(Sort.Sort2(left,right,data)));

    }


    public static int[] Sort1(int[] data) {


        int[] dataNew = new int[data.length];
        int A = data.length;
        for (int a = data.length - 1; a > 0; a--) {

            for (int i = data.length - 1; i > A - (data.length); i--) {


                if (data[i] < data[i - 1]) {
                    dataNew[i] = data[i - 1];
                    data[i - 1] = data[i];
                    data[i] = dataNew[i];
                } else {
                    data[i] = data[i];


                }


            }
            A++;


        }
        return data;
    }

    public static int[] Sort2(int[]left,int[] right,int[] tab) {
        int a = 0;
        int b = 0;
        int i = 0;
        tab = new int[2*left.length];
        while (b < left.length && a < right.length) {
            if (left[a] <= right[b]) {
                tab[i] = left[a];
                a++;
            } else {
                tab[i] = right[b];
                b++;
            }

            i++;
        }

        while (a < left.length) {
            tab[i] = left[a];
            i++;
            a++;

        }
        while (b < right.length) {
            tab[i] = right[b];
            i++;
            b++;

        }


        return tab;
    }

    public static int[] Sortx(int[] tab) {

        int[] D = new int[tab.length];
        int S = 0;
        int A = 0;
        for (int i = 0; i < tab.length ; i++) {
            S=tab[A];
            for (int a = A; a < tab.length-2 ; a++) {

                if (S < tab[a+1]) {
                    S= S;
                }
                else{
                    S = tab[a+1];
                }

            }

            D[A] = S;

            A++;

        }
        return D;
    }

}