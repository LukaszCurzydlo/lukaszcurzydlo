package Sportsman;


public class BasicSportsman implements Sportsman {



    public void prepare() {
        System.out.println("Rozgrzewka...");
    }


    public void doPumps(int pumps) {

        for (int i = 0; i < pumps; i++)
            System.out.println("Robie pompke nr " + (i + 1));

    }


    public void doSquats(int squats) {

        for (int i = 0; i < squats; i++)
            System.out.println("Robie przysiad nr " + (i + 1));

    }


    public void doCrunches(int crunches) {

        for (int i = 0; i < crunches; i++)
            System.out.println("Robie brzuszek nr " + (i + 1));

    }
}
