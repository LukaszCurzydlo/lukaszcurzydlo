package Sportsman;


public class Selfiemaking implements Sportsman {

    private Sportsman sportsman;

    public Selfiemaking(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    public void prepare() {
        sportsman.prepare();
        selfie();
    }




    public void doPumps(int pumps) {

        sportsman.doPumps(pumps);
        selfie();

    }


    public void doSquats(int squats) {
        sportsman.doSquats(squats);
        selfie();

    }


    public void doCrunches(int crunches) {
        sportsman.doCrunches(crunches);
        selfie();

    }

    private void selfie() {
        System.out.println("Time to take a selfie!");
    }

}
