package Sportsman;


public class DoubleSeries implements Sportsman {

    private Sportsman sportsman;

    public DoubleSeries(Sportsman sportsman) {
        this.sportsman = sportsman;

    }



    public void prepare() {

        sportsman.prepare();

    }


    public void doPumps(int pumps) {

        pumps *= 2;
        sportsman.doPumps(pumps);


    }


    public void doSquats(int squats) {

        squats *= squats;
        sportsman.doSquats(squats);

    }


    public void doCrunches(int crunches) {

        crunches *= 2;
        sportsman.doCrunches(crunches);

    }
}