package Sportsman;


public class Waterdrinking implements Sportsman {

    private Sportsman sportsman;

    public Waterdrinking(Sportsman sportsman) {
        this.sportsman = sportsman;
    }


    public void prepare() {

        sportsman.prepare();
        drinkingWater();

    }




    public void doPumps(int pumps) {

        sportsman.doPumps(pumps);
        drinkingWater();

    }


    public void doSquats(int squats) {

        sportsman.doSquats(squats);
        drinkingWater();

    }


    public void doCrunches(int crunches) {

        sportsman.doCrunches(crunches);
        drinkingWater();

    }

    private void drinkingWater() {
        System.out.println("Pije wode.");
    }
}