package Sportsman;


public class NoPreparee implements Sportsman {

    private Sportsman sportsman;

    public NoPreparee(Sportsman sportsman) {
        this.sportsman = sportsman;
    }


    public void prepare() {


    }


    public void doPumps(int pumps) {

        sportsman.doPumps(pumps);

    }


    public void doSquats(int squats) {

        sportsman.doSquats(squats);

    }


    public void doCrunches(int crunches) {

        sportsman.doCrunches(crunches);

    }
}
