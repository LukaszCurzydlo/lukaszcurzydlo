package Office;

import java.util.LinkedList;
import java.util.List;


public class Author {

private int id;
private String name;


    private List<Float> list = new LinkedList();

    public Author(int id, String name, List<Float> list) {
        this.id = id;
        this.name = name;
        this.list = list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Float> getList() {
        return list;
    }

    public void setList(List<Float> list) {
        this.list = list;
    }
}
