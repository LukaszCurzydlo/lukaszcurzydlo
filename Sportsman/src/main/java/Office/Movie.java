package Office;

import java.util.LinkedList;
import java.util.List;

public class Movie {


   private int id;
   private int MovieLength;
   private String Title;
   private List<Float> list = new LinkedList();
   private int date;


    public Movie(int id, int movieLength, String title, List<Float> list, int date) {
        this.id = id;
        MovieLength = movieLength;
        Title = title;
        this.list = list;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieLength() {
        return MovieLength;
    }

    public void setMovieLength(int movieLength) {
        MovieLength = movieLength;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public List<Float> getList() {
        return list;
    }

    public void setList(List<Float> list) {
        this.list = list;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
}
