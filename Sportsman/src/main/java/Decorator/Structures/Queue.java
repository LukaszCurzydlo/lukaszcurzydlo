package Decorator.Structures;

import java.util.Arrays;

/**
 * Created by RENT on 2017-06-27.
 */
public class Queue implements SimpleQueue {
    private int number = 0;
    private int[] data = new int[10];
    private int index = 0;

    public void D() {
        System.out.println(Arrays.toString(data));
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void offer(int value) {
        if (number < data.length) {
            if (index == data.length) {
                index = 0;
            }
            data[index] = value;
            index++;
            number++;


        } else {
            if (index == data.length) {
                index = 0;
            }
            int[] Newdata = new int[2 * data.length];
            int S = data.length;
            for (int i = 0; i < data.length; i++) {
                Newdata[i] = data[index];
                index++;
                if (index == data.length) {
                    index = 0;
                }
            }

            data = Newdata;
            index = S ;
            offer(value);
        }


    }

    @Override
    public int poll() {
        return 0;
    }


    @Override
    public int peek() {
        return 0;
    }


}
