package Decorator.Structures;

/**
 * Created by RENT on 2017-06-23.
 */
public class SimpleArrayList implements SimpleList {
    private int number = 0;
    private int[] data = new int[100];

    public void add(int value) {
        if (number < data.length) {
            data[number] = value;
            number++;
        }else {
            arrayTooSmall();
            return;
        }
    }

    private void arrayTooSmall() {
        throw new RuntimeException("Za mało miejsca w liście.");
    }

    public int get(int index) {
        if (index < number) {
            return data[index];
        }else {
            throw new IndexOutOfBoundsException();
        }
    }

    public void add(int value, int index) {
        if (number < data.length) {
            if (index < number) {
                for (int i = number; i > index; i--) {
                    data[i] = data[i - 1];
                }
                data[index] = value;
            } else {
                throw new IndexOutOfBoundsException();
            }
            number++;
        } else {
            arrayTooSmall();
        }
    }

    public boolean contain(int value) {
        for(int i = 0 ; i < number ; i++) {
            if (data[i] == value) {
                return true;
            }
        }
        return false;
    }

    public void remove(int index) {
        if (index < number && index >= 0) {
            for (int i = index; i < number-1; i++) {
                data[i] = data[i + 1];
            }
            number--;
        } else {
            throw new IndexOutOfBoundsException("Zły indeks");
        }
    }

    public void removeValue(int value) {
        for (int i = 0; i < number; i++) {
            if (data[i] == value) {
                remove(i);
                break;
            }
        }
    }

    public int size() {
        return number;
    }


}
