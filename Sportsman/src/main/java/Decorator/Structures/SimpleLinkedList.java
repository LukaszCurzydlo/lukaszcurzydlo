package Decorator.Structures;

/**
 * Created by RENT on 2017-06-26.
 */
public class SimpleLinkedList implements SimpleList {


    private Element first;
    private Element last;
    private int number;

    @Override
    public void add(int value) {
        Element element = new Element(value);
        if (number == 0) {
            first = element;
        } else {
            last.next = element;
            element.prev = last;
        }

        last = element;
        number++;
    }

    @Override
    public int get(int index) {
        if (index < number) {
            Element element = find(index);
            return element.value;
        }else {
            throw new IndexOutOfBoundsException();
        }
    }

    private Element find(int index) {
        Element element = first;
        for (int i = 0; i < index; i++) {
            element = element.next;
        }
        return element;
    }

    @Override
    public void add(int value, int index) {
        if (index == number) {
            add(value);
        }else if (index == 0){
            Element newElement = new Element(value);
            newElement.value = value;
            newElement.next = first;
            first.prev = newElement;
            first = newElement;
        }
        else if(index < number) {
            Element newElement = new Element(value);
            Element left = find(index-1);
            Element right = left.next;
            left.next = newElement;
            newElement.prev = left;
            right.prev = newElement;
            newElement.next = right;
            number++;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public boolean contain(int value) {
        Element element = first;
        while (element != null) {
            if (element.value == value) {
                return true;
            }
            element = element.next;
        }
        return false;
    }

    @Override
    public void remove(int index) {
        if (index == 0) {
            if (number == 1) {
                first = null;
                last = null;
            }
            else {
                first = first.next;
                first.prev = null;
            }
        }else if (index == number -1) {
            last = last.prev;
            last.next = null;
        }else if (index < number) {
            Element left = find(index - 1);
            Element right = left.next.next;

            left.next = right;
            right.prev  = left;
        }else {
            throw new IndexOutOfBoundsException();
        }
        number--;
    }

    @Override
    public void removeValue(int value) {
        Element element = first;
        int index = 0;
        while (element != null) {
            if (element.value == value) {
                remove(index);
                break;
            }
            element = element.next;
            index++;
        }
    }

    @Override
    public int size() {
        return number;
    }

    private static class Element {
        int value;
        Element next;
        Element prev;

        public Element(int value) {
            this.value = value;
        }
    }
}
