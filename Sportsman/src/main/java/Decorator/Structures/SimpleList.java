package Decorator.Structures;

/**
 * Created by RENT on 2017-06-23.
 */
public interface SimpleList {
    void  add(int value);
    void add(int value,int index);
    boolean contain(int value);
    void remove(int index);
    void removeValue(int index);
    int size();
int get (int a);


}
