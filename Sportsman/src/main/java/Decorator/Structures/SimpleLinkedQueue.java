package Decorator.Structures;

import java.util.*;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueue implements SimpleQueue {

    private Element first;
    private Element last;
    private int number;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(int a) {
        Element element = new Element(a);
        number++;
        if (isEmpty()) {

            first = element;
            last = element;
        }
        else{
            last.next = element;
             last = element ;
        }

    }



    @Override
    public int poll() {
        if (!isEmpty()) {
            if (number > 1) {
                int result = peek();
                first = first.next;
                return result;
            } else {
                int result = peek();
                first = null;
                last = null;
                return result;
            }
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public int peek() {
        if (!isEmpty()) {
            return first.value;
        }
        else throw new NoSuchElementException();
    }

    private static class Element {
        int value;
        Element next;
        public Element(int value) {
            this.value = value;
        }
    }








}
