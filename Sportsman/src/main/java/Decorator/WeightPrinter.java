package Decorator;

import java.io.PrintStream;


public class WeightPrinter implements PersonPrinter {


    private PersonPrinter personprinter;

    WeightPrinter(PersonPrinter personprinter){
        this.personprinter = personprinter;
    }

    public void print(Person person, PrintStream out) {
        personprinter.print(person, out);
        out.println(person.getWeight());


    }
}
