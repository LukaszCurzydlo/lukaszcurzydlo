package Decorator;

import java.io.PrintStream;


public class Ageprinter implements PersonPrinter {

    private PersonPrinter personprinter;

    Ageprinter(PersonPrinter personprinter){
        this.personprinter = personprinter;
    }


    public void print(Person person, PrintStream out) {
        personprinter.print(person, out);
        out.println(person.getEyesColor());
    }
}
