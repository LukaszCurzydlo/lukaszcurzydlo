package Decorator;



public class Main {

    public static void main(String[] args) {
        PersonPrinter per = new WeightPrinter(new Ageprinter(new BasicDataPrinter()));
        Person p = new Person("luk","luk",10,10,10,"2");
        per.print(p,System.out);

    }
}
