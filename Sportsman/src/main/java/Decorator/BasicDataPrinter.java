package Decorator;

import java.io.PrintStream;


public class BasicDataPrinter implements PersonPrinter {


    public void print(Person person, PrintStream out) {
        out.println("name : " + person.getName());
        out.println("surname : " + person.getSurname());
    }

}
