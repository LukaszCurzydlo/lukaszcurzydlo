package Decorator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;



    public class FilePrinter {
        private final PersonPrinter personPrinter;

        public FilePrinter(PersonPrinter personPrinter) {
            this.personPrinter = personPrinter;
        }

        public void printToFile(Person person, String filename) {
            try (PrintStream out = new PrintStream("file.txt")){
                personPrinter.print(person,out);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

