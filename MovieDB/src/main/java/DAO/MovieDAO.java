package DAO;

import Entity.Movie;
import org.hibernate.Session;
import org.hibernate.Transaction;
import HibernateUtil.HibernateUtil;
import java.util.List;


public class MovieDAO implements AbstractDAO<Movie>{


        public boolean insert(Movie type) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            int newId = Integer.valueOf(session.save(type) + "");
            System.out.println("Umieściłem rekord o ID = " + newId);
            t.commit();
            session.close();
            return true;
        }

        public boolean delete(Movie type) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.delete(type);
            t.commit();
            if(this.get(type.getId()) == null) {
                return true;
            }
            session.close();
            return false;
        }

        public boolean delete(int id) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.delete(this.get(id));
            t.commit();
            session.close();
            return true;
        }

        public boolean update(Movie type) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.update(type);
            t.commit();
            session.close();
            return true;
        }

        public Movie get(int id) {
            Movie movie;
            Session session = HibernateUtil.openSession();
            movie = session.load(Movie.class, id);
            session.close();
            return movie;
        }

        public List<Movie> get() {
            List<Movie> movies;
            Session session = HibernateUtil.openSession();
            movies = session.createQuery("from Movie").list();
            session.close();
            return movies;
        }


}

