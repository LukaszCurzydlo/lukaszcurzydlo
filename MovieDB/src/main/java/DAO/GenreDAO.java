package DAO;

import Entity.Genre;
import Entity.Movie;
import HibernateUtil.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;


public class GenreDAO implements  AbstractDAO<Genre> {

    public boolean insert(Genre type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        int newId = Integer.valueOf(session.save(type) + "");
        System.out.println("Umieściłem rekord o ID = " + newId);
        t.commit();
        session.close();
        return true;
    }

    public boolean delete(Genre type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        if(this.get(type.getId()) == null) {
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(int id) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        session.close();
        return true;
    }

    public boolean update(Genre type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        session.close();
        return true;
    }

    public Genre get(int id) {
        Genre movie;
        Session session = HibernateUtil.openSession();
        movie = session.load(Genre.class, id);
        session.close();
        return movie;
    }

    public List<Genre> get() {
        List<Genre> movies;
        Session session = HibernateUtil.openSession();
        movies = session.createQuery("from Genre").list();
        session.close();
        return movies;
    }
}
