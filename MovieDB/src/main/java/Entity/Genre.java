package Entity;


import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;


@Entity
@Table(name = "genre")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="name")
private String name;


    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Movie> movieSet = new LinkedList<Movie>();



    public List<Movie> getMovieSet() {
        return movieSet;
    }

    public Genre setMovieSet(List<Movie> movieSet) {
        this.movieSet = movieSet;
        return this;
    }

    public Genre() {
    }

    public Genre(String name) {

        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Genre setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Genre setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", movieSet=" + movieSet +
                '}';
    }
}
