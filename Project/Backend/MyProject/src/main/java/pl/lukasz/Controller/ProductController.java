package pl.lukasz.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.lukasz.Entity.MealProduct;
import pl.lukasz.Entity.Product;
import pl.lukasz.Repository.MealProductRepository;
import pl.lukasz.Repository.ProductRepository;

import java.util.List;



@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MealProductRepository mealProductRepository;


    @RequestMapping("/show")
    public List<Product> listProducts() {
        return (List<Product>) productRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Product showProductByID(@PathVariable("id") long id) {
        return productRepository.findOne(id);
    }

    @RequestMapping("/add")
    public Product addProduct(@RequestParam(name = "name") String name,
                              @RequestParam(name = "protein") double protein,
                              @RequestParam(name = "fat") double fat,
                              @RequestParam(name = "carbohydrates") double carbohydrates,
                              @RequestParam(name = "calories") double calories
    )
    {

        Product pr = new Product();
        pr.setName(name);
        pr.setProtein(protein);
        pr.setFat(fat);
        pr.setCarbohydrates(carbohydrates);
        pr.setCalories(calories);
        return productRepository.save(pr);


    }

    @RequestMapping("/edit")
    public Product EditProduct(            @RequestParam(name = "id") long id,
                                          @RequestParam(name = "name") String name,
                                          @RequestParam(name = "protein") double protein,
                                          @RequestParam(name = "fat") double fat,
                                          @RequestParam(name = "carbohydrates") double carbohydrates,
                                          @RequestParam(name = "calories") double calories
    )
    {
        Product pr = new Product();
        pr.setId(id);
        pr.setName(name);
        pr.setProtein(protein);
        pr.setFat(fat);
        pr.setCarbohydrates(carbohydrates);
        pr.setCalories(calories);
        return productRepository.save(pr);

    }

    @RequestMapping("/delete/{id}")
    public boolean deleteMeal(@PathVariable("id") long id) {
        Product product = productRepository.findOne(id);
        for(MealProduct m : product.getMealProductsList()) {
            mealProductRepository.delete(m);
        }
        productRepository.delete(product);
        return true;
    }




}


