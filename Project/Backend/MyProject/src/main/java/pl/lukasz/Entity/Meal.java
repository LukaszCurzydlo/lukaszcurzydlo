package pl.lukasz.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;



@Entity
public class Meal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    @JsonBackReference
    @ManyToMany(mappedBy = "meals")
    private List<DailyMealPlan> dailyMealPlans;

    @JsonIgnoreProperties("meal")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.meal", cascade=CascadeType.ALL)
    private List<MealProduct> mealProductList;


    public Meal() {
    }

    public List<DailyMealPlan> getDailyMealPlans() {
        return dailyMealPlans;
    }

    public void setDailyMealPlans(List<DailyMealPlan> dailyMealPlans) {
        this.dailyMealPlans = dailyMealPlans;
    }


    public List<MealProduct> getMealProductList() {
        return mealProductList;
    }

    public void setMealProductList(List<MealProduct> mealProductList) {
        this.mealProductList = mealProductList;
    }


    public Meal(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}