package pl.lukasz.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.lukasz.Entity.*;
import pl.lukasz.Repository.DailyMealPlanRepository;
import pl.lukasz.Repository.MealProductRepository;
import pl.lukasz.Repository.MealRepository;
import pl.lukasz.Repository.ProductRepository;
import java.util.List;



@CrossOrigin
@RestController
@RequestMapping("/meal")
public class MealController {



    @Autowired
    private MealProductRepository mealProductRepository;
    @Autowired
    private DailyMealPlanRepository dailyMealPlanRepository;
    @Autowired
    private MealRepository mealRepository;
    @Autowired
    private ProductRepository productRepository;


    @RequestMapping("/show")
    public List<Meal> listMeals() {
        return (List<Meal>) mealRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Meal showMealByID(@PathVariable("id") long id) {
        return mealRepository.findOne(id);
    }

    @RequestMapping("/add")
    public Meal add(@RequestParam("name") String name) {
        Meal m = new Meal(name);
        return mealRepository.save(m);
    }

    @RequestMapping("/add/{id}")
    public Meal addProductToMeal(@PathVariable("id") long id,
                                 @RequestParam("productId") long productId,
                                 @RequestParam("amount") double amount) {
        Meal meal = mealRepository.findOne(id);
        Product product = productRepository.findOne(productId);
        meal.getMealProductList().add(new MealProduct(new MealProductId(meal, product),amount));
        return mealRepository.save(meal);
    }




    @RequestMapping("/delete/{id}")
    public boolean deleteMeal(@PathVariable("id") long id) {
        Meal meal = mealRepository.findOne(id);
        for(DailyMealPlan dmp : meal.getDailyMealPlans()) {

            for(Meal m : dmp.getMeals()) {
                if(m.getId() == id)
                    dmp.getMeals().remove(m);
                break;
            }
            dailyMealPlanRepository.save(dmp);
        }
        mealRepository.delete(meal);
        return true;
    }






    @RequestMapping("/edit")
   public boolean deleteProductFromMeal(@RequestParam(name = "id") long id,
                                        @RequestParam(name = "productId") long productId) {
        Meal meal = mealRepository.findOne(id);
        List<MealProduct> products = meal.getMealProductList();
        for(MealProduct mp : products){

            if(mp.getProduct().getId() == productId){
                products.remove(mp);
                break;
            }

        }
        meal.setMealProductList(products);
        mealRepository.save(meal);
        Product p = productRepository.findOne(productId);
        MealProduct mp = new MealProduct();
        mp.setProduct(p);
        mp.setMeal(meal);
        mealProductRepository.delete(mp);
        return true;

    }



}
