package pl.lukasz.Repository;

import org.springframework.data.repository.CrudRepository;
import pl.lukasz.Entity.MealProduct;


public interface MealProductRepository extends CrudRepository<MealProduct, Long> {
}
