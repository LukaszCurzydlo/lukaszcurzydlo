package pl.lukasz.Repository;

import org.springframework.data.repository.CrudRepository;
import pl.lukasz.Entity.Product;


public interface ProductRepository extends CrudRepository<Product, Long> {


}
