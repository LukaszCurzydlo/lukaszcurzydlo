package pl.lukasz.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.lukasz.Repository.DailyMealPlanRepository;
import pl.lukasz.Repository.MealRepository;

import javax.persistence.*;
import java.util.List;




@Entity
public class DailyMealPlan {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;


    @JsonManagedReference
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Meal> meals;


    public DailyMealPlan(List<Meal> meals) {
        this.meals = meals;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public DailyMealPlan setMeals(List<Meal> meals) {
        this.meals = meals;
        return this;
    }

    public DailyMealPlan(String name) {
        this.name = name;
    }

    public DailyMealPlan() {
    }

    public long getId() {
        return id;
    }

    public DailyMealPlan setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public DailyMealPlan setName(String name) {
        this.name = name;
        return this;
    }
}