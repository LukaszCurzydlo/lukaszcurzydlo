package pl.lukasz.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.lukasz.Entity.DailyMealPlan;
import pl.lukasz.Entity.Meal;
import pl.lukasz.Entity.Product;
import pl.lukasz.Repository.DailyMealPlanRepository;
import pl.lukasz.Repository.MealRepository;
import java.util.List;



@CrossOrigin
@RestController
@RequestMapping("/plan")
public class DailyMealPlanController {

    @Autowired
    private DailyMealPlanRepository dailyMealPlanRepository;

    @Autowired
    private MealRepository mealRepository;

    @RequestMapping("/show")
    public List<DailyMealPlan> listPlans() {
        return (List<DailyMealPlan>) dailyMealPlanRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public DailyMealPlan showPlanByID(@PathVariable("id") String id) {
        return dailyMealPlanRepository.findOne(Long.valueOf(id));
    }


    @RequestMapping("/add")
    public DailyMealPlan addPlan (@RequestParam(name = "name") String name) {
        DailyMealPlan pl = new DailyMealPlan();
        pl.setName(name);
        return dailyMealPlanRepository.save(pl);
    }


    @RequestMapping("/add/{id}")
    public DailyMealPlan addMealToMealPlan(@PathVariable("id") long id,
                                          @RequestParam(name = "mealId") long mealId) {

        DailyMealPlan dailyMealPlan =  dailyMealPlanRepository.findOne(id);
        Meal meal = mealRepository.findOne(mealId);
        List<Meal> list = dailyMealPlan.getMeals();
        list.add(meal);
        dailyMealPlan.setMeals(list);
        return dailyMealPlanRepository.save(dailyMealPlan);
    }




    @RequestMapping("/delete/{id}")
    public boolean deleteMealPlan(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        DailyMealPlan dailyMealPlan =  dailyMealPlanRepository.findOne(myId);
        dailyMealPlan.setMeals(null);
         dailyMealPlanRepository.delete(dailyMealPlan);
         return  true;
    }

    @RequestMapping("/edit/{id}")
    public boolean deleteMealFromMealPlan(@PathVariable("id") long id,
                                       @RequestParam(name = "meal") long mealId ){


        DailyMealPlan dailyMealPlan = dailyMealPlanRepository.findOne(id);
        List<Meal> meals = dailyMealPlan.getMeals();
        meals.remove(mealRepository.findOne(mealId));
        dailyMealPlan.setMeals(meals);
        dailyMealPlanRepository.save(dailyMealPlan);
        return true;
    }






}
