package pl.lukasz.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;



@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private double protein;
    private double fat;
    private double carbohydrates;
    private double calories;



    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.product")
    private List<MealProduct> mealProductsList;



    public Product(long id ,String name, double protein, double fat, double carbohydrates, double calories) {
        this.id = id;
        this.name = name;
        this.protein = protein;
        this.fat = fat;
        this.carbohydrates = carbohydrates;
        this.calories = calories;
    }





    public Product() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public List<MealProduct> getMealProductsList() {
        return mealProductsList;
    }

    public void setMealProductsList(List<MealProduct> mealProductsList) {
        this.mealProductsList = mealProductsList;
    }
}
