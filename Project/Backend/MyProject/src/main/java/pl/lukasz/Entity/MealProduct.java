package pl.lukasz.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;



    @Entity
    @AssociationOverrides({
            @AssociationOverride(name = "pk.meal",
                    joinColumns = @JoinColumn(name = "meal_id")),
            @AssociationOverride(name = "pk.product",
                    joinColumns = @JoinColumn(name = "product_id")) })
    public class MealProduct {
        @EmbeddedId
        @JsonIgnore
        private MealProductId pk = new MealProductId();
        private double amount;

        public MealProduct() {
        }

        public MealProduct(MealProductId pk, double amount) {
            this.pk = pk;
            this.amount = amount;
        }

        public MealProductId getPk() {
            return pk;
        }

        public void setPk(MealProductId pk) {
            this.pk = pk;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public Meal getMeal() {
            return pk.getMeal();
        }

        public void setMeal(Meal meal) {
            pk.setMeal(meal);
        }

        public Product getProduct() {
            return pk.getProduct();
        }

        public void setProduct(Product product) {
            pk.setProduct(product);
        }
    }

