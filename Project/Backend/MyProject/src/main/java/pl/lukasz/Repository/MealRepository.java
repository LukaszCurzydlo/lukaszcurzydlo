package pl.lukasz.Repository;

import org.springframework.data.repository.CrudRepository;
import pl.lukasz.Entity.Meal;


public interface MealRepository  extends CrudRepository<Meal, Long> {

}
