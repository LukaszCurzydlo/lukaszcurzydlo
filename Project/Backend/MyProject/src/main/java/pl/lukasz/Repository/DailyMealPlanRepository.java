package pl.lukasz.Repository;

import org.springframework.data.repository.CrudRepository;
import pl.lukasz.Entity.DailyMealPlan;


public interface DailyMealPlanRepository  extends CrudRepository<DailyMealPlan, Long> {
}
