package pl.lukasz.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;



    @Embeddable
    public class MealProductId implements Serializable {
        @ManyToOne
        private Meal meal;
        @ManyToOne
        @JsonBackReference
        private Product product;

        public MealProductId() {

        }

        public MealProductId(Meal meal, Product product) {
            this.meal = meal;
            this.product = product;
        }

        public Meal getMeal() {
            return meal;
        }

        public void setMeal(Meal meal) {
            this.meal = meal;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }
    }

