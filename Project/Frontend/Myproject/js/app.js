var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8080/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
    
        .when('/', {
            templateUrl: path + 'main.html'
        }).when('/type/add', {
            templateUrl: path + 'typeAdd.html',
            controller: 'typeAddController'
        }).when('/type/show', {
            templateUrl: path + 'typeShow.html',
            controller: 'typeShowController'
        }).when('/type/delete/:id', {
            templateUrl: path + 'typeDeleteId.html',
            controller: 'typeDeleteIdController'
        }).when('/product/show', {
            templateUrl: path + 'productShow.html',
            controller: 'productShowController'
        }).when('/product/show/:id', {
            templateUrl: path + 'productShowId.html',
            controller: 'productShowIdController'
        }).when('/product/delete/:id', {
            templateUrl: path + 'productDeleteId.html',
            controller: 'productDeleteIdController'
        }).when('/product/edit/:id', {
            templateUrl: path + 'productEdit.html',
            controller: 'productEditController'
        }).when('/product/show/:id', {
            templateUrl: path + 'productShowId.html',
            controller: 'productShowIdController'
        }).when('/product/add', {
            templateUrl: path + 'productAdd.html',
            controller: 'productAddController'
        }).when('/meal/show/:id', {
            templateUrl: path + 'mealShowId.html',
            controller: 'mealShowIdController'
        }).when('/meal/show', {
            templateUrl: path + 'mealShow.html',
            controller: 'mealShowController'
        }).when('/meal/add/:id', {
            templateUrl: path + 'mealAddIdProductId.html',
            controller: 'mealAddIdProductController'
        }).when('/meal/add', {
            templateUrl: path + 'mealAdd.html',
            controller: 'mealAddController'
        }).when('/meal/delete/:id', {
            templateUrl: path + 'mealDeleteId.html',
            controller: 'mealDeleteIdController'
        }).when('/meal/edit/:id', {
            templateUrl: path + 'mealEdit.html',
            controller: 'mealEditController'
        }).when('/plan/add/:id', {
            templateUrl: path + 'planAddIdMealMealId.html',
            controller: 'planAddIdMealMealIdController'
        }).when('/plan/delete/:id', {
            templateUrl: path + 'planDeleteId.html',
            controller: 'planDeleteIdController'
        }).when('/plan/edit/:id', {
            templateUrl: path + 'planEdit.html',
            controller: 'planEditController'
        }).when('/plan/show/:id', {
            templateUrl: path + 'planShowId.html',
            controller: 'planShowIdController'
        }).when('/plan/show', {
            templateUrl: path + 'planShow.html',
            controller: 'planShowController'
        }).when('/plan/add', {
            templateUrl: path + 'planAdd.html',
            controller: 'planAddController'
        })
  
    ;
});



app.controller('planEditController', function($scope, $http, $routeParams) {

  
    $http({
        url: url + 'plan/show/' + $routeParams.id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.plan = succ.data;
    }, function(err) {
        console.error(err);
    });
    
       

    
    $scope.mealRemoveFromPlan = function() {
        $http({
            url: url + 'plan/edit/'+$routeParams.id,
            dataType: 'json',
            params: {
                meal: $scope.mealId
            }
        }).then(function(succ) {
            var z = [];
            for(prod of $scope.plan.meals) {
                if(prod.id == $scope.mealId) {
                    continue;
                }
                z.push(prod);
            }
            $scope.plan.meals = z;
        }, function(err) {
            console.error(err);
        });
    }
})





app.controller('planShowController', function($scope, $http) {
    $http({
        url: url + 'plan/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.plans = success.data;
    }, function(error) {
        console.error(error);
    });
});



app.controller('planShowIdController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $scope.names = [];

    $http({
        url: url + 'plan/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.plan = success.data;
    }, function(error) {
        console.error(error);
    });
    
     $http({
        url: url+ 'plan/show/' + id,
    }).then(function(succ) {
        var plan = succ.data;
        var planMeals = plan.meals;
         
        plan.values = {
            calories: 0,
            carbohydrates: 0,
            fat: 0,
            protein: 0
        };
         for(var t of planMeals){
        $scope.names.push(t.name);

        for(var p of t.mealProductList) {
            var amount = parseInt(p.amount/100);
            plan.values.calories += amount * parseInt(p.product.calories);
            plan.values.carbohydrates += amount * parseInt(p.product.carbohydrates);
            plan.values.fat += amount * parseInt(p.product.fat);
            plan.values.protein += amount * parseInt(p.product.protein);
        }
         }
         $scope.ValuesOfMeal = plan.values;
    }, function(err) {
        console.log(err);
    });
    
    
    
});


app.controller('planAddIdMealMealIdController', function($scope, $http,$routeParams) {
    
    
    $http({
        url: url + 'plan/show/' + $routeParams.id,
        dataType: 'json'
    }).then(function(success) {
        $scope.plan = success.data;
    }, function(err) {
        console.error(err);
    });
    
    
    
$http({
        url: url + 'meal/show'
    }).then(function(success) {
        $scope.dishes = success.data;
    }, function(error) {
        console.error(error);
    });
    


    $scope.DishAdd = function() {
        $http({
            url: url + 'plan/add/'+$routeParams.id,
            dataType: 'json',
            params: {
                mealId: $scope.dish
            }
        }).then(function(success) {
            if(success.data.id > 0) {
        $scope.plan = success.data;
            } else
                $scope.message = "Wystąpił błąd podczas dodawania posiłku.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('planAddController', function($scope, $http, $location) {
    
    
$http({
        url: url + 'plan/show'
    }).then(function(success) {
        $scope.Plans = success.data;
    }, function(error) {
        console.error(error);
    });
    
        
    
    $scope.planAdd = function() {
        $http({
            url: url + 'plan/add',
            dataType: 'json',
            params: {
                name: $scope.namePlan

            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $location.path("/plan/add/" + success.data.id);
            } else
                $scope.message = "Wystąpił błąd podczas dodawania planu.";
        }, function(error) {
            console.error(error);
        });
    }
});


            




app.controller('planDeleteIdController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'plan/delete/' + id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.message = "Usunięto poprawnie.";
    }, function(err) {
        console.error(err);
    });
});




app.controller('mealDeleteIdController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'meal/delete/' + id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.message = "Usunięto poprawnie.";
    }, function(err) {
        console.error(err);
    });
});





app.controller('mealAddController', function($scope, $http, $location) {

    
         $http({
        url: url + 'meal/show'
    }).then(function(success) {
        $scope.meals = success.data;
    }, function(error) {
        console.error(error);
    });
    
    $scope.MealAdd = function() {
        $http({
            url: url + 'meal/add',
            dataType: 'json',
            params: {
                name: $scope.mealName
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $location.path("/meal/add/" + success.data.id);
            } else
                $scope.message = "Wystąpił błąd podczas dodawania posiłku.";
        }, function(error) {
            console.error(error);
        });
    }
});










app.controller('mealAddIdProductController', function($scope, $http,$routeParams) {
    $scope.productList = [];
    var id = $routeParams.id;

    $http({
        url: url + 'product/show',
    }).then(function(success) {
        $scope.products = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
  $http({
        url: url + 'meal/show/' + $routeParams.id,
    }).then(function(success) {
        $scope.meal = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
   
    
    $scope.productAddToMeal = function() {
        $http({  
            url: url + 'meal/add/'+$routeParams.id,    
            dataType: 'json',
            params: {
                id: $scope.id,
                productId: $scope.productId,
                amount: $scope.amount
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.meal =success.data;
                console.log($scope.productList);
            } else
                $scope.message = "Wystąpił błąd podczas dodawania produktu.";
        }, function(error) {
            console.error(error);
        });
    }
});



app.controller('mealEditController', function($scope, $http, $routeParams,$window) {
         var id = $routeParams.id;
    $scope.prod = [];

     
    
     $http({
        url: url+ 'meal/show/' + id,
    }).then(function(succ) {
        var meal = succ.data;
        var mealProducts = meal.mealProductList;
        for(var p of mealProducts) {
            $scope.prod.push(p.product);
        }
    }, function(err) {
        console.log(err);
    });
    
    
    
    $scope.productRemoveFromMeal = function() {
        $http({
            url: url + 'meal/edit',
            dataType: 'json',
            params: {
                id: $routeParams.id,
                productId: $scope.productId
            }
        }).then(function(success){
            $window.location.href = "/#!meal/edit/" + id;
        }, function(error) {
            console.error(error);
        });
    }
});




app.controller('mealShowIdController', function($scope, $http, $routeParams) {
   var id = $routeParams.id;
    
    
     $http({
        url: url + 'meal/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.meal = success.data;
    }, function(error) {
        console.error(error);
    });
    
     $http({
        url: url+ 'meal/show/' + id,
    }).then(function(succ) {
        var meal = succ.data;
        var mealProducts = meal.mealProductList;
        meal.values = {
            calories: 0,
            carbohydrates: 0,
            fat: 0,
            protein: 0
        };
         
        var names = [];
         
        for(var p of mealProducts) {
            var amount = parseInt(p.amount/100);
            meal.values.calories += amount * parseInt(p.product.calories);
            meal.values.carbohydrates += amount * parseInt(p.product.carbohydrates);
            meal.values.fat += amount * parseInt(p.product.fat);
            meal.values.protein += amount * parseInt(p.product.protein);
            names.push(p.product.name);
        
        }
      $scope.ArrayOfNames = names;
        meal.total = meal.values;
         $scope.ValuesOfMeal = meal.total;
    }, function(err) {
        console.log(err);
    });
    
    
});






app.controller('mealShowController', function($scope, $http) {
    $http({
        url: url + 'meal/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.meals = success.data;
    }, function(error) {
        console.error(error);
    });
});






app.controller('productEditController', function($scope, $http, $routeParams) {

     var id = $routeParams.id;
    $http({
        url: url + 'product/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.product = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
    $scope.productEdit = function() {
        $http({
            url: url + 'product/edit/',
            dataType: 'json',
            params: {
                id: $routeParams.id,
                name: $scope.product.name,
                protein: $scope.product.protein,
                fat: $scope.product.fat,
                carbohydrates: $scope.product.carbohydrates,
                calories: $scope.product.calories

            }
        }).then(function(succ) {
         $scope.message = "Produkt zmodyfikowano poprawnie."
        }, function(err) {
            console.error(err);
        });
    }
});





app.controller('productAddController', function($scope, $http) {
    
    $http({
        url: url + 'product/show'
    }).then(function(success) {
        $scope.products = success.data;
    }, function(error) {
        console.error(error);
    });
    
        
    
    $scope.productAdd = function() {
        $http({
            url: url + 'product/add',
            dataType: 'json',
            params: {
                name: $scope.productName,
                protein: $scope.protein,
                fat: $scope.fat,
                carbohydrates: $scope.carbohydrates,
                calories: $scope.calories
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.products.push(success.data);
                $scope.message = "Produkt dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania produktu.";
        }, function(error) {
            console.error(error);
        });
    }
});












app.controller('productDeleteIdController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'product/delete/' + id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.message = "Usunięto poprawnie.";
    }, function(err) {
        console.error(err);
    });
});




app.controller('productShowController', function($scope, $http) {
    $http({
        url: url + 'product/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.products = success.data;
    }, function(error) {
        console.error(error);
    });
});





app.controller('productShowIdController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'product/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.product = success.data;
    }, function(error) {
        console.error(error);
    });
});








app.controller('typeShowController', function($scope, $http) {
    $http({
        url: url + 'type/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.types = success.data;
    }, function(error) {
        console.error(error);
    });
});





app.controller('typeAddController', function($scope, $http) {
    
    $http({
        url: url + 'type/show'
    }).then(function(success) {
        $scope.types = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
    $scope.typeAdd = function() {
        $http({
            url: url + 'type/add',
            dataType: 'json',
            params: {
                name: $scope.type
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.types.push(success.data);
                $scope.message = "Typ posiłku dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania typu posiłku.";
        }, function(error) {
            console.error(error);
        });
    }
});




app.controller('typeDeleteIdController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'type/delete/' + id,
        dataType: 'json'
    }).then(function(succ) {
        $scope.message = "Usunięto poprawnie.";
    }, function(err) {
        console.error(err);
    });
});














