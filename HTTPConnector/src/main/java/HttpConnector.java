import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import static sun.net.www.protocol.http.HttpURLConnection.userAgent;


public class HttpConnector {


    public static void main(String[] args) {
HttpConnector h = new HttpConnector();
        try {
            String s = h.sendPOST("http://palo.ferajna.org/sda/wojciu/json.php","login=admin");
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(h);
    }


private URL obj;
private HttpURLConnection con;
private String ua;

    public HttpConnector( URL obj, HttpURLConnection con, String ua) {

        this.obj = obj;
        this.con = con;
        this.ua = ua;
    }


    public HttpConnector() {
    }

    public URL getObj() {
        return obj;
    }

    public HttpConnector setObj(URL obj) {
        this.obj = obj;
        return this;
    }

    public HttpURLConnection getCon() {
        return con;
    }

    public HttpConnector setCon(HttpURLConnection con) {
        this.con = con;
        return this;
    }

    public String getUa() {
        return ua;
    }

    public HttpConnector setUa(String ua) {
        this.ua = ua;
        return this;
    }




    public   String sendGET(String url) throws IOException {
        this.obj = new URL(url);
        this.con = (HttpURLConnection) obj.openConnection();
        String userAgent ="Greg/1.0";

        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", userAgent);

        int responseCode = con.getResponseCode();
        String result = "";
        if(responseCode == 200){

            InputStream res = con.getInputStream();
            InputStreamReader reader = new InputStreamReader(res);
            Scanner sc = new Scanner(reader);
            while(sc.hasNextLine()){
                result += sc.nextLine();
            } sc.close();
            System.out.println(result);
        }
        return result;
    }


    public String sendPOST(String url,String params) throws IOException {
        this.obj = new URL(url);
        this.con = (HttpURLConnection) obj.openConnection();
        String userAgent ="Greg/1.0";

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", userAgent);

        con.setDoOutput(true);

        DataOutputStream dos = new DataOutputStream(con.getOutputStream());
        dos.writeBytes(params);
        dos.flush();
        dos.close();

        Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
        String lines = "";
        while(sc.hasNextLine()) {
            lines += sc.nextLine();
        }
        sc.close();

        return lines;
    }

}
